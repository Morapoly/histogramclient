# HistogramClient



## Getting started

The purpose is to develop a simple gui chart view which plots a histogram of receiving data due to local host.

Use ```DataGeneratorServer.exe``` as a data provider and develop a data receiver using socket programming.

Receiving data should be classified to 20 clusters; for example receiving numbers in range of ```[0 - 100]``` should be displayed with bars that every bar provides statistical frequency for every splitted range (```[0 - 5]``` , ```[5 - 10]```, ...) .

## Tips
- Receive data via udp socket with ```localHost (127.0.0.1)``` address and port ```8090```.
- The refresh rate can be custom between ```50 ms``` and ```1 second```.
- The rate of receiving data is ```1000 numbers per second```.
- The range of receiving data is fixed to ```[0 - 100]```.
- You can develop your app using ```QT widgets``` or ```QML ChartView```.
- ```Zoom or pan gesturs``` and using ```SOLID principles``` are recommended but not necessary.

## After you finished
Generate execution file of your app (```exe file```) and integrate it with your source codes and send them all to email ```ungmoradi@gmail.com``` also you can request member invitation and create new branch and push your code and exe file to this repo instead of sending to the email.
